#Reading sensors app

##Description
This App was developed in cordova. to fast read
sensors and send reading information via bluetooth
or websockets to a server where sensor information is used.


##Requeriments
* Node.js
* Apache Cordova
* JDK Java
* SDK Android
* Device with developer options

##Project Installation
* Clone this repository
* Installation of dependences "npm install"
* restore platforms and plugins "cordova prepare"

##Run Project

###Browser to test
* Run project in browser "cordova run browser"
* Go to URL localhost

###Android
* Build project to Android "cordova build android"
* Run project in Android "cordova run android"