var container = document.getElementById('container');
distance = 0
datos = 0
arrayDatos = []
connect = false
macAddress = "98:D3:32:F5:A1:88"
lastDatos = []
typeMove = "scrollTo" /*Options: container to scrollLeft or window scrollTo*/
sizeImage = window.localStorage.getItem("heightImage") ? window.localStorage.getItem("heightImage") : 838
speedMove = window.localStorage.getItem("speedMove") ? window.localStorage.getItem("speedMove") : 6


document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {

  nfc.addNdefListener(NFCHandler(), function (a) { console.log("success, ", a) }, function (a) { console.log("failed, ", a) });
  bluetoothSerial.connectInsecure(macAddress , blueToothHandler, blueToothFailHandler);

}

function NFCHandler() {
  nfc.addTagDiscoveredListener(function (nfcEvent) {
      texto = 'connected to ' + nfc.bytesToHexString(nfcEvent.tag.id)
      // texto = nfcEvent.tag,
      //     ndefMessage = texto.ndefMessage;
      nfc.connect('android.nfc.tech.NfcA', 500).then( (a) => {

              Swal.fire({
              title: 'Conectado!!',
              text: 'Tag detectado !!',
              type: 'success',
              confirmButtonText: 'OK'
            }),
          console.log(texto, a)
          
          }).catch( (a) => {
          console.log('paila ' , a)
      });
  })
}

function blueToothFailHandler (a) {
  console.log("NOT", a)

  Swal.fire(
    'Fallo de Conexión!',
    'No se conecto, por favor revisar que el dispositivo este encendido y reiniciar aplicación"',
    'error'
  )
}

function blueToothHandler(a) {
  console.log("YES", a);

  Swal.fire(
    'Conexion Satisfactoria!',
    'Se ha logrado conectar',
    'success'
  )

  setInterval(function () {
    bluetoothSerial.read(function (data) {
      // console.log(data)
      datos = data
      arrayDatos = datos.split("/")
      console.log(arrayDatos)
      calculateDistance(arrayDatos)
      console.log("last", lastDatos)
      distance = Math.round(averageFifo(lastDatos))
      console.log("distance: ", distance)

      console.log(defineTypeMove(typeMove))
      console.log("Dimension: ", sizeImage)
    })
  }, 300) //Tiempo que tarda para volver a ejecutar
}

//Function calculate distance with new array 
function calculateDistance(newArray) {
  lastDatos = newArray.slice(newArray.length - 6, newArray.length - 1)
}

//Function calculateFifo of array
function calculateFifo(newValue) {
  numbers.push(newValue)
  numbers.length > 5 ? numbers.shift() : numbers.length == 5 ? averageFifo(numbers) : numbers
  return numbers
}

//Function calculate average of new array
function averageFifo(number) {
  total = 0
  for (i = 0; i < 5; i++) {
    total += (number[i] / 10)
    console.log("total ", total)
  }
  return total / 5
}

//Function define type of scroll
function defineTypeMove(type) {
  switch (type) {
    case "scrollTo":
      window.scrollTo(0, (sizeImage - (distance * speedMove)))
      return window.scrollY
    case "scrollLeft":
      container.scrollLeft = (sizeImage - (distance * speedMove))
      return container.scrollLeft
  }
}

//Function to configure size and velocity of movement
function configureSize() {
  (async function getNewValue() {
    const { value: newDimension } = await Swal.fire({
      title: 'Enter dimensions of image',
      input: 'number',
      inputValue: sizeImage,
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          window.localStorage.clear()
          sizeImage = 838
        }
      }
    })

    if (newDimension) {
      Swal.fire(`The new dimension is ${newDimension}`)
      window.localStorage.setItem("heightImage", newDimension)

      if(window.localStorage.getItem("heightImage")){
        sizeImage = window.localStorage.getItem("heightImage")
      }
      // sizeImage = newDimension
    }
  })()
}

function configureSpeed(){
  (async function getNewValue() {
    const { value: newSpeed } = await Swal.fire({
      title: 'Enter velocity of movement',
      input: 'number',
      inputValue: speedMove,
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          window.localStorage.clear()
          speedMove = 6
        }
      }
    })

    if (newSpeed) {
      Swal.fire(`The new dimension is ${newSpeed}`)
      window.localStorage.setItem("speedMove", newSpeed)

      if(window.localStorage.getItem("speedMove")){
        speedMove = window.localStorage.getItem("speedMove")
      }
      // sizeImage = newSpeed
    }
  })()
}








